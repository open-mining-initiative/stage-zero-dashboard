# Stage Zero Dashboard

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Data science dashboard for proof-of-work mining analytics, markov chains, and pool revenue.

## Table of Contents

- [Install](#install)
- [Deployment](#deployment)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [Development Notes](#notes)
- [License](#license)

## Install

### Install python poetry

`curl -sSL https://install.python-poetry.org | python3 -`

### Install the project and run it

> Install all dependencies
`poetry install`

> Activate a shell inside the virtual environment
`poetry shell`

> Finally, run the app
`szero_dashboard`

## Deployment

```
# TODO
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## Notes

### .env file

the .env file has an `APP_ENV` variable which takes two values of either `dev` or `prod`

This application is made with pure python and some pretty impressive data science libraries.

### Dash

1. Defines the looks of the application using the apps layout module
2. Uses callbacks to determine which parts of the application are interactive
   and what they will react to.
   
   
#### Modules

1. dash -- Responsible for initializing the application

2. dash_core_components -- Allows the creation of interactive components like graphs,
   dropdowns, or date ranges etc..

3. dash_html_components -- Allows access to html tags

## License

* [APGL © 2022 Matt Williams](LICENSE)
