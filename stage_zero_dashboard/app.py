"""
Root app
"""

# 3rd party
import dash

# package
from stage_zero_dashboard import Config
from stage_zero_dashboard.appshell import create_appshell
from stage_zero_dashboard.logger.pkg_logger import Logger

Log = Logger().get_logger()

SCRIPTS = [
    "https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.10.8/dayjs.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.10.8/locale/ru.min.js",
    "https://www.googletagmanager.com/gtag/js?id=G-4PJELX1C4W",
]


def run():
    Log.info(
        f"Staring {Config.package()} v{Config.version()} in {Config.env()} mode")
    App = dash.Dash(
        Config.package(),
        use_pages=True,
        external_scripts=SCRIPTS,
        update_title=None,
    )
    App.layout = create_appshell(dash.page_registry.values())
    App.run(debug=Config.debug())


if __name__ == "__main__":
    run()
