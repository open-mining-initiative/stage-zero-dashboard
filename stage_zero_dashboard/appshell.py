"""
Build the Application dashboard shell
"""
# standard lib
from typing import List, Tuple

# 3rd party
import dash_mantine_components as dmc
from dash import Input, Output, clientside_callback, dcc, html, page_container
from dash_iconify import DashIconify


def create_table_of_contents(toc_items: List[Tuple[str]]) -> dmc.Navbar:
    children = []
    for url, name, _ in toc_items:
        children.append(
            dmc.Anchor(
                name,
                style={"textTransform": "capitalize",
                       "textDecoration": "none"},
                href=url,
                size="sm",
                color="gray",
            )
        )

    return dmc.Navbar(
        id="toc-navbar",
        position={"top": 100, "right": 0},
        fixed=True,
        width={"base": 300},
        style={"paddingRight": 20},
        children=[
            dmc.Text(
                "Table of Contents",
                style={
                    "marginBottom": 10
                },
                weight=500
            ),
            dmc.Group(direction="column", spacing=0, children=children),
        ],
    )


def create_home_link(label: str) -> dmc.Text:
    return dmc.Text(
        children=[
            label,
        ],
        size="xl",
        color="gray",
    )


def create_header_navbar(nav_data) -> dmc.Header:
    return dmc.Header(
        height=70,
        fixed=True,
        p="md",
        children=[
            dmc.Container(
                fluid=True,
                style={"paddingRight": 12, "paddingLeft": 12},
                children=dmc.Group(
                    position="apart",
                    align="flex-start",
                    children=[
                        dmc.Center(
                            dcc.Link(
                                [
                                    dmc.MediaQuery(
                                        create_home_link(
                                            "Stage Zero Dashboard"),
                                        smallerThan="sm",
                                        styles={"display": "none"},
                                    ),
                                    dmc.MediaQuery(
                                        create_home_link("Stage Zero"),
                                        largerThan="sm",
                                        styles={"display": "none"},
                                    ),
                                ],
                                href="/",
                                style={"paddingTop": 3,
                                       "textDecoration": "none"},
                            ),
                        ),
                        dmc.Group(
                            position="right",
                            align="center",
                            spacing="xl",
                            children=[
                                dmc.MediaQuery(
                                    dmc.Select(
                                        id="select-component",
                                        style={"width": 250},
                                        placeholder="Search",
                                        nothingFound="No match found",
                                        searchable=True,
                                        clearable=True,
                                        data=[
                                            component["name"]
                                            for component in nav_data
                                            if component["name"]
                                            not in ["Home", "Not found 404"]
                                        ],
                                        icon=[
                                            DashIconify(
                                                icon="radix-icons:magnifying-glass"
                                            )
                                        ],
                                    ),
                                    smallerThan="md",
                                    styles={"display": "none"},
                                ),
                                html.A(
                                    dmc.ThemeIcon(
                                        DashIconify(
                                            icon="radix-icons:github-logo",
                                            width=22,
                                        ),
                                        radius=30,
                                        size=36,
                                        variant="outline",
                                        color="gray",
                                    ),
                                    href="https://github.com/gohash-software/dashboard-analytics",
                                ),
                                html.A(
                                    dmc.ThemeIcon(
                                        DashIconify(
                                            icon="bi:discord",
                                            width=22,
                                            color="#7289da",
                                        ),
                                        radius=30,
                                        size=36,
                                        variant="outline",
                                    ),
                                    href="https://discord.gg/AFkMcQYe",
                                ),
                                dmc.ThemeSwitcher(
                                    id="color-scheme-toggle",
                                    style={"cursor": "pointer"},
                                ),
                            ],
                        ),
                    ],
                ),
            )
        ],
    )


def create_main_nav_link(icon: str, label: str, href: str):
    return dcc.Link(
        dmc.Group(
            [
                dmc.ThemeIcon(
                    DashIconify(icon=icon, width=18),
                    size=40,
                    radius=40,
                    variant="light",
                ),
                dmc.Text(label, size="sm", color="gray"),
            ]
        ),
        href=href,
        style={"textDecoration": "none"},
    )


def create_side_navbar() -> dmc.MediaQuery:
    main_links = dmc.Group(
        direction="column",
        spacing="md",
        children=[
            create_main_nav_link(
                icon="radix-icons:home",
                label="Home",
                href="/",
            ),
            create_main_nav_link(
                icon="radix-icons:ruler-square",
                label="Pool Analysis",
                href="/pool-analysis",
            ),
            create_main_nav_link(
                icon="radix-icons:id-card",
                label="News",
                href="/news",
            ),
            create_main_nav_link(
                icon="radix-icons:backpack",
                label="Learn",
                href="/learn",
            ),
        ],
    )

    children = [
        dmc.Group(
            grow=True,
            position="left",
            spacing="sm",
            direction="column",
            style={"paddingLeft": 20, "paddingRight": 20, "marginTop": 5},
            children=[main_links] + [dmc.Space(h=20)],
        ),
    ]

    return dmc.Navbar(
        id="components-navbar",
        fixed=True,
        position={"top": 70},
        width={"base": 225},
        children=[
            dmc.ScrollArea(
                offsetScrollbars=True,
                type="scroll",
                children=children,
            ),
        ],
    )


def create_appshell(nav_data) -> dmc.MantineProvider:
    return dmc.MantineProvider(
        id="theme-provider",
        theme={
            "colorScheme": "dark",
            "fontFamily": "'Inter', sans-serif",
            "primaryColor": "indigo",
        },
        styles={
            "Button": {"root": {"fontWeight": 400}},
            "Alert": {"title": {"fontWeight": 500}},
            "AvatarsGroup": {"truncated": {"fontWeight": 500}},
        },
        withGlobalStyles=True,
        withNormalizeCSS=True,
        children=[
            dmc.NotificationsProvider(
                [
                    create_header_navbar(nav_data),
                    create_side_navbar(),
                    dcc.Location(id="url"),
                    html.Div(
                        id="wrapper",
                        children=dmc.Container(
                            id="main-content",
                            size="lg",
                            pt=90,
                            children=page_container,
                        ),
                    ),
                    html.Div(
                        id="dummy-container-for-header-select",
                        style={"display": "none"},
                    ),
                ]
            ),
        ],
    )


clientside_callback(
    """function(colorScheme) {
        return {
            colorScheme,
            fontFamily: "'Inter', sans-serif",
            primaryColor: "indigo"
        }
    }""",
    Output("theme-provider", "theme"),
    Input("color-scheme-toggle", "value"),
    prevent_initial_callback=True,
)

# noinspection PyProtectedMember
clientside_callback(
    """
    function(children) {
        return null
    }
    """,
    Output("select-component", "value"),
    Input("_pages_content", "children"),
)

clientside_callback(
    """
    function(value) {
        if (value) {
            document.getElementById(value).click()
        }
        return value
    }
    """,
    Output("dummy-container-for-header-select", "children"),
    Input("select-component", "value"),
)
