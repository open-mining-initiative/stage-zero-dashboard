"""
Interface for a Component
"""

# standard library
import abc


class IComponent(metaclass=abc.ABCMeta):
    """
    @desc: Interface that all components must impl
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'mount') and
                callable(subclass.mount) or
                NotImplemented)

    @abc.abstractmethod
    def mount(self):
        """
        @desc: Mounts the component
        """
        raise NotImplementedError
