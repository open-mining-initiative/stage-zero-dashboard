"""
Render a graph
"""

# standard lib
import random

# 3rd party
import plotly.graph_objects as go
import dash_mantine_components as dmc
from dash import dcc
from pandas import DataFrame

# package
from stage_zero_dashboard.logger.pkg_logger import Logger

Log = Logger().get_logger()

TEMPLATE = "plotly_dark"


def BitcoinGraph(props: DataFrame) -> dcc.Graph | None:
    """
    @desc: Graph historical bitcoin data
    """
    try:
        figure = go.Figure({
            "data": [
                go.Candlestick(x=props["Date"],
                               open=props["Open"],
                               high=props["High"],
                               low=props["Low"],
                               close=props["Close"])
            ],
            "layout": go.Layout(
                paper_bgcolor="rgba(0,0,0,0)",
                plot_bgcolor="rgba(0,0,0,0)",
                template=TEMPLATE,
            ),
        })

        figure.update_layout(xaxis_rangeslider_visible=False)

        return dcc.Graph(
            figure=figure,
            config={
                "displaylogo": False,
                "fillFrame": False,
            },
        )

    except KeyError as error:
        Log.error(f"Error, key does not exist: {error}")

    return None


def NetworkHashRateGraph(props: DataFrame) -> dcc.Graph | None:
    """
    @desc: Graph
    """
    try:
        figure = go.Figure({
            "data": [
                go.Line(
                    x=props["Date"],
                    y=props["hash-rate"]
                )
            ],
            "layout": go.Layout(
                paper_bgcolor="rgba(0,0,0,0)",
                plot_bgcolor="rgba(0,0,0,0)",
                template=TEMPLATE,
            ),
        })

        return dcc.Graph(
            figure=figure,
            config={
                "displaylogo": False,
                "fillFrame": False,
            },
        )
    except KeyError as error:
        Log.error(f"Error, key does not exist: {error}")

    return None


def MiningPoolHashRateGraph() -> dcc.Graph | None:
    """
    @desc: Display known mining pools and their hashrate, hardcoded for now
    """
    try:
        figure = go.Figure({
            "data": [
                go.Bar(
                    x=list(range(10)),
                    y=[random.randint(200, 1000) for _ in range(10)],
                    name="Slush Pool",
                    marker={"line": {"width": 0}},
                    marker_color=dmc.theme.DEFAULT_COLORS["gray"][5],
                ),
                go.Bar(
                    x=list(range(10)),
                    y=[random.randint(200, 1000) for _ in range(10)],
                    name=u"Poolin",
                    marker={"line": {"width": 0}},
                    marker_color=dmc.theme.DEFAULT_COLORS["indigo"][4],
                ),
                go.Bar(
                    x=list(range(10)),
                    y=[random.randint(200, 1000) for _ in range(10)],
                    name=u"Foundry",
                    marker={"line": {"width": 0}},
                    marker_color=dmc.theme.DEFAULT_COLORS["blue"][7],
                ),
                go.Bar(
                    x=list(range(10)),
                    y=[random.randint(200, 1000) for _ in range(10)],
                    name=u"Ant Pool",
                    marker={"line": {"width": 0}},
                    marker_color=dmc.theme.DEFAULT_COLORS["red"][4],
                ),
                go.Bar(
                    x=list(range(10)),
                    y=[random.randint(200, 1000) for _ in range(10)],
                    name=u"Terra Pool",
                    marker={"line": {"width": 0}},
                    marker_color=dmc.theme.DEFAULT_COLORS["green"][4],
                ),
            ],
            "layout": go.Layout(
                paper_bgcolor="rgba(0,0,0,0)",
                plot_bgcolor="rgba(0,0,0,0)",
                template=TEMPLATE,
                xaxis={"showgrid": True, "zeroline": True, "visible": True},
                yaxis={"showgrid": True, "zeroline": True, "visible": True},
                showlegend=True,
            ),
        })

        return dcc.Graph(
            figure=figure,
            config={
                "displaylogo": False,
                "fillFrame": False,
            },
        )
    except KeyError as error:
        Log.error(f"Error, key does not exist: {error}")

    return None
