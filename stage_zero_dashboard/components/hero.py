"""
The welcome text to the main page
"""
# 3rd party
from dash import html

# package


def Hero(header: str, desc: str):
    return html.Section(
        children=[
            html.H1(children=header,
                    className="display-6 text-center mt-4"),
            html.P(
                children=desc,
                className="lead text-center mb-5"
            ),
        ]
    )
