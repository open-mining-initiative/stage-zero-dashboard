"""
This component creates a simple sidebar layout using inline style arguments and the
dbc.Nav component.

dcc.Location is used to track the current location, and a callback uses the
current location to render the appropriate page content. The active prop of
each NavLink is set automatically according to the current pathname. To use
this feature you must install dash-bootstrap-components >= 0.11.0.

For more details on building multi-page Dash applications, check out the Dash
documentation: https://dash.plot.ly/urls
"""

# standard lib

# 3rd party
# import dash
# import dash_bootstrap_components as dbc
# from dash import Input, Output, dcc, html

# package
from stage_zero_dashboard.components import IComponent


class SideNav(IComponent):
    """
    @desc: This feature is too complex to implement right now.
    """

    def __init__(self) -> None:
        ...

    def mount(self):
        raise NotImplementedError
