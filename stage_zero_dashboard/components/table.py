"""
A table which has mutable data fields
"""
# standard lib

# 3rd party
# import dash_mantine_components as dmc
import dash_bootstrap_components as dbc
from dash import html

# package
from stage_zero_dashboard import IComponent


class BitcoinNetworkDataTable(IComponent):
    """
    @desc: Displays all relavent bitcoin network network statistics
    card.
    """

    def __init__(self) -> None:
        # TODO: Move all hard coded data out of the mount method,
        # and into the data module, and pass it in to this constructor
        # as arguments.
        super().__init__()

    def mount(self) -> html.Section:
        """
        @desc: Builds a table of mutable hashrate data
        @return: html.Section
        """
        table_header = [
            html.Thead(
                html.Tr(
                    [
                        html.Th("Price (USD)"),
                        html.Th("Network Hashrate"),
                        html.Th("Block Reward"),
                        html.Th("Tx Fees"),
                        html.Th("Daily Blocks"),
                        html.Th("Daily Btc"),
                        html.Th(children="Monthly Btc"),
                    ]
                )
            )
        ]

        # assign a value to each table header
        table_header_row_values = html.Tr(
            children=[
                html.Td("$19,416"),     # btc price
                html.Td("280.387 E/H"),       # network hashrate
                html.Td("6.25"),      # block reward
                html.Td("0.07"),      # transaction fees
                html.Td("144"),       # daily blocks
                html.Td("910.08"),    # total daily bitcoin
                html.Td("27,300"),    # total monthly bitcoin
            ])

        table_body = [
            html.Tbody(
                children=[
                    table_header_row_values,
                ])]

        return html.Section(
            children=[
                dbc.Card(
                    class_name="mb-3",
                    children=[
                        dbc.CardHeader("Bitcoin Network"),
                        dbc.CardBody(
                            children=[
                                html.H4("Mining Statistics",
                                        className="card-title text-center"),
                                dbc.Table(
                                    key="0",
                                    dark=False,
                                    color="light",
                                    children=table_header + table_body,
                                    bordered=True,
                                    striped=True,
                                    borderless=True,
                                    hover=True,
                                    responsive=True,
                                )
                            ]
                        ),  # CardBody
                        dbc.CardFooter(
                            children=[
                                dbc.Button(
                                    color="primary",
                                    size="sm",
                                    children=[
                                        "Refresh Data"
                                    ],
                                )
                            ]
                        ),  # Card Footer
                    ]
                )  # Card
            ]
        )


class PoolProfitabilityTable(IComponent):
    """
    @desc: Displays all relavent statistics to calculated a pools profit
    """

    def __init__(self) -> None:
        # TODO: Move all hard coded data out of the mount method,
        # and into the data module, and pass it in to this constructor
        # as arguments.
        super().__init__()

    def mount(self) -> html.Section:
        """
        @desc: Builds a table of pool profitablity metrics
        @return: html.Section
        """
        table_header = [
            html.Thead(
                html.Tr(
                    children=[
                        html.Th("Blocks Found"),
                        html.Th("Btc revenue"),
                        html.Th("Pools Fees (BTC)"),
                        html.Th("Pool Fees (USD)"),
                    ]
                )
            )
        ]

        # assign a value to each table header
        table_header_row_values = html.Tr(
            children=[
                html.Td("0.668571429"),   # blocks found
                html.Td("4.225371429"),   # bitcoin reward
                html.Td("0.169014857"),   # pool fees (btc)
                html.Td("$3,211.28"),     # pool fees (usd)
            ]
        )

        table_body = [
            html.Tbody(
                children=[
                    table_header_row_values,
                ]
            )
        ]

        return html.Section(
            children=dbc.Card(
                class_name="mb-3",
                children=[
                    dbc.CardHeader(children="Pool Profitablity"),
                    dbc.CardBody(
                        children=[
                            html.H4("Metrics",
                                    className="card-title text-center"),
                            dbc.Table(
                                key="0",
                                dark=False,
                                color="light",
                                children=table_header + table_body,
                                bordered=True,
                                striped=True,
                                borderless=True,
                                hover=True,
                                responsive=True,
                            )
                        ]
                    ),  # CardBody
                    dbc.CardFooter(
                        children=[
                            dbc.Button(
                                color="primary",
                                size="sm",
                                children=[
                                    "Calculate"
                                ],
                            )
                        ]
                    ),  # Card Footer
                ]
            )  # Card
        )
