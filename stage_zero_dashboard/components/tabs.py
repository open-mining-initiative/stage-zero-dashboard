"""
Renders tabular layout for components
"""

# standard lib

# 3rd party
import dash_mantine_components as dmc
from pandas import DataFrame
from dash import html

# package
from stage_zero_dashboard.components.graph import (
    BitcoinGraph, NetworkHashRateGraph, MiningPoolHashRateGraph)


def TabbedContent(btc_data: DataFrame, hashrate_data: DataFrame):
    """
    @desc: dynamically rendered tab content containing graphs
    """
    return html.Section(
        children=dmc.Tabs(
            orientation="horizontal",
            children=[
                dmc.Tab(
                    children=BitcoinGraph(btc_data),
                    label="Bitcoin Market",
                ),
                dmc.Tab(
                    children=NetworkHashRateGraph(hashrate_data),
                    label="Network Hashrate",
                ),
                dmc.Tab(
                    children=MiningPoolHashRateGraph(),
                    label="Distributed Pool Hashrate",
                )
            ]
        )
    )
