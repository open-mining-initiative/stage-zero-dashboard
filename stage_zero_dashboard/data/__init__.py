"""
All data for components and pages go here.
This can be used for custom css styles, or text that should be passed
to components as props.
"""
from typing import List, Dict


NAV_LOGO = "https://images.plot.ly/logo/new-branding/plotly-logomark.png"

# Navbar links
NAV_ITEM_DATA: List[Dict[str, str]] = [
    {
        "key": "0",
        "name": "Home",
        "path": "/",
    },
    {
        "key": "1",
        "name": "Pool Analysis",
        "path": "/pools",
    },
    {
        "key": "2",
        "name": "News",
        "path": "/news",
    },
]

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
SIDEBAR_CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}
