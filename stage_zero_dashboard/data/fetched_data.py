"""
Remote data classes
"""

# standard lib
import time
from enum import Enum
from urllib.error import URLError
from urllib.request import urlopen
from urllib.request import HTTPError

# package
from stage_zero_dashboard.logger.pkg_logger import Logger

# 3rd party
import pandas as pd
from pandas import DataFrame


class Currencies(Enum):
    BTC = "BTC-USD"
    ETHW = "ETHW-USD"
    DOGE = "DOGE-USD"
    LTC = "LTC-USD"


class Interval(Enum):
    DAILY = "1d"
    WEEKLY = "1wk"
    MONTHLY = "1m"


class RemoteData:
    def __init__(self, currency: Currencies = Currencies.BTC) -> None:
        self._currency = currency.value
        self._log = Logger().get_logger()
        self._valid = False
        self._data: DataFrame = pd.DataFrame()
        self.refreshData()
        self._log.debug(self._data)

    def refreshData(self, startDate: int = int(time.time()), endDate: int = int(time.time())-(31554000*5), interval: Interval = Interval.DAILY):
        """
        @desc: Get data from the Web
        @param startDate: Where data starts, aka the closest date from now(default: now)
        @param endDate: Furthest date to grab data(default: five years ago)
        @param interval: The interval between each data point(default: weekly)
        """
        if startDate < endDate:
            return

        url = "https://query1.finance.yahoo.com/v7/finance/download/" + self._currency + "?period1=" + str(
            endDate) + "&period2=" + str(startDate) + "&interval=" + interval.value + "&events=history&includeAdjustedClose=true"
        try:
            with urlopen(url) as file:
                self._data = pd.read_csv(file)
                self._log.info("Data successfully fetched from: " + url)
                self._valid = True
        except URLError or HTTPError as error:
            self._log.error("Could not fetch data from: " +
                            url + "\n" + "\tError Code: " + str(error))

    @property
    def data(self) -> DataFrame:
        """
        @desc: Fetch the actuall data
        """
        return self._data

    @property
    def currency(self) -> Currencies:
        """
        @desc: Gets the currency of the current data
        """
        return self._currency

    @property
    def isDataValid(self) -> bool:
        return self._valid
