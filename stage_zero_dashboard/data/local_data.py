"""
Data classes
"""

# standard lib

# package
from stage_zero_dashboard import Config
from stage_zero_dashboard.logger.pkg_logger import Logger

# 3rd party
import pandas as pd
from pandas import DataFrame


class HashRateData:
    """
    @desc: Loads the bitcoin network hashrate data from disk
    """

    def __init__(self) -> None:
        self._log = Logger().get_logger()
        self._data: DataFrame = pd.read_csv(Config.btc_hashrate_data())

    @property
    def data(self) -> DataFrame:
        """
        @desc: getter for the internal pandas data frame
        """
        return self._data
