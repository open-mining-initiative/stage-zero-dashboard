"""
Set up a thread safe singleton meta class
"""

# standard lib
from typing import Any, Dict

# package
from stage_zero_dashboard import Config

LOGGING_CONFIG: Dict[str, Any] = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "colored": {
            "()": "colorlog.ColoredFormatter",
            "format": "%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s",
        },
        "simple": {"format": "%(levelname)s - %(message)s"},
        "pedantic": {
            "format": "%(asctime)s - %(module)s - %(funcName)s - %(name)s - %(lineno)d - %(levelname)s - %(message)s"
        },
    },
    "handlers": {
        "console-color": {
            "class": "logging.StreamHandler",
            "formatter": "colored",
            "stream": "ext://sys.stdout",
        },
        "file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "filename": f"{Config.log_dir() / Config.logfile_name()}",
            "formatter": "pedantic",
        },
    },
    "loggers": {
        # dev
        "dev": {
            "handlers": [
                "console-color",
                "file_handler",
            ],
            "level": "DEBUG",
            "propagate": False,
        },
        "prod": {
            "handlers": [
                "file_handler",
            ],
            "level": "INFO",
            "propagate": False,
        },
    },
    "root": {"handlers": ["file_handler"]},
}
