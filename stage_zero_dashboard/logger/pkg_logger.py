"""
Customized system wide logger
"""

# standard lib
import logging
import sys
from logging.config import dictConfig
from pathlib import Path

# package
from stage_zero_dashboard import Config, ThreadSafeMeta
from stage_zero_dashboard.logger import LOGGING_CONFIG


class Logger(metaclass=ThreadSafeMeta):
    """
    A singleton logger for application wide use
    """

    def __init__(self):
        self.__log_dump: Path = Config.log_dir()
        self._logging_config = LOGGING_CONFIG
        self.__load_config()

    def __load_config(self):
        """
        @desc: Load the config dictionary
        @desc: meta private:
        @return none
        """
        # if dump site doesn't exist, create it,
        # and all parent folders leading up to it
        if not self.__log_dump.is_dir():
            self.__log_dump.mkdir(parents=True)

        # remove the log file on load if it already exists
        log_file_path = Config.log_dir() / Config.logfile_name()
        if log_file_path.exists():
            log_file_path.unlink()

        try:
            # if syntax is wrong, logging module will raise ValueError,
            # catch, and exit execution
            dictConfig(self._logging_config)
        except ValueError as error:
            sys.stderr.write(
                f"Loading default logging config failed, syntax error\n\n{error}"
            )
            sys.exit(1)
        except KeyError as error:
            sys.stderr.write(
                f"Loading logging config failed, syntax error\n\n{error}"
            )
            sys.exit(1)

    def get_logger(self) -> logging.Logger:
        """
        Return a logger by name.
        Only logger names that are defined in the config
        file will be used
        @return: an instance of Logger configured by custom params
        """
        # first test to see if the name is a valid defined logger name
        valid: bool = False
        try:
            for logger_name in self._logging_config["loggers"]:
                if logger_name == Config.env():
                    valid = True
        except KeyError as error:
            sys.stderr.write(f"{error}")

        if not valid:
            # name passed is not a valid listed logger,
            # return dev as default logger
            sys.stderr.write(
                f"\n{Config.env()}: Is not a valid logger\n"
                f"Falling back to: {Config.default_env()}\n")
            logger = logging.getLogger(Config.default_env())
            return logger

        # name was valid
        logger = logging.getLogger(Config.env())
        return logger
