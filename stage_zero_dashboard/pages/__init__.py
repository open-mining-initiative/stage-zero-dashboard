"""
Interface for a Page
"""

# standard library
import abc


class IPage(metaclass=abc.ABCMeta):
    """
    @desc: Interface that all pages must impl
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'render') and
                callable(subclass.render) or
                NotImplemented)

    @abc.abstractmethod
    def render(self):
        """
        @desc: Renders the entire page
        """
        raise NotImplementedError
