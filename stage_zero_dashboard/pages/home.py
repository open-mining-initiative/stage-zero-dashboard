"""
Dashboard homepage
"""

# standard lib

# 3rd party
import dash
import dash_mantine_components as dmc
from dash import dcc, html
from dash_iconify import DashIconify

# package

dash.register_page(
    __name__,
    "/",
    title="Stage Zero Dashboard",
    description="Research and Development dashboard for mining pool analysis, revenue streams, and crypto mining news and price updates",
)


def create_title(title: str, id: str) -> dmc.Text:
    return dmc.Text(title, align="center", style={"fontSize": 30}, id=id)


def create_head(text: str) -> dmc.Text:
    return dmc.Text(text, align="center", style={"margin": "10px 0"})


def Tile(icon: str, heading: str, description: str) -> html.Section:
    return html.Section(
        children=[
            dmc.Paper(
                p="lg",
                withBorder=True,
                children=[
                    dmc.Group(
                        direction="column",
                        spacing=0,
                        align="center",
                        children=[
                            dmc.ThemeIcon(
                                children=[
                                    DashIconify(
                                        icon=icon,
                                        height=30,
                                        color="dark"
                                    ),
                                ],
                                size=50,
                                radius=50,
                                variant="outline",
                            ),
                            dmc.Text(
                                children=[
                                    heading,
                                ],
                                style={"marginTop": 20, "marginBottom": 10}
                            ),
                            dmc.Text(
                                children=[
                                    description,
                                ],
                                color="dimmed",
                                align="center",
                                size="sm",
                                style={"lineHeight": 1.6, "marginBottom": 15},
                            ),
                        ],
                    ),
                ],
            )
        ]
    )


layout = html.Section(
    children=[
        dmc.Container(
            size="lg",
            style={"marginTop": 30},
            children=[
                create_title(
                    "Data Science and Market Research for the Proof-of-Work Sector",
                    id="features",
                ),
                create_head(
                    "Analyze data, forecast pool profitablity, keep the mining world at your finger tips"),
                dmc.Group(
                    [
                        dcc.Link(
                            [
                                dmc.Button("Learn about mining"),
                            ],
                            href="/learn",
                        ),
                        html.A(
                            dmc.Button(
                                "Join Discord",
                                variant="outline",
                                leftIcon=[DashIconify(
                                    icon="bi:discord", width=20)],
                            ),
                            href="https://discord.gg/AFkMcQYe",
                        ),
                        html.A(
                            dmc.Button(
                                "Github",
                                variant="outline",
                                color="gray",
                                leftIcon=[
                                    DashIconify(
                                        icon="radix-icons:github-logo", width=20
                                    )
                                ],
                            ),
                            href="https://github.com/gohash-software/dashboard-analytics",
                        ),
                    ],
                    position="center",
                    style={"marginTop": 20, "marginBottom": 90},
                ),
            ],
        ),
        dmc.Container(
            size="lg",
            px=0,
            py=0,
            children=[
                dmc.SimpleGrid(
                    cols=3,
                    breakpoints=[
                        {"maxWidth": "xs", "cols": 1},
                        {"maxWidth": "sm", "cols": 2},
                    ],
                    children=[
                        Tile(
                            icon="radix-icons:dashboard",
                            heading="Work with live data",
                            description="Keep an eye on bitcoins price, the network hashrate, and competitor pools hashrate"
                        ),
                        Tile(
                            icon="radix-icons:mix",
                            heading="Calculate mining pool profitablity",
                            description="Forecast your mining operations revenue over time with historical, live, or your own data"
                        ),
                        Tile(
                            icon="radix-icons:id-card",
                            heading="News Aggregator",
                            description="Our web scrapers crawl crypto mining platforms, brining you the latest in mining"
                        ),
                        Tile(
                            icon="radix-icons:twitter-logo",
                            heading="Social Media Feeds",
                            description="Live twitter and reddit updates from prominent figures in the mining community"
                        ),
                        Tile(
                            icon="radix-icons:blending-mode",
                            heading="Dark Mode",
                            description="Like any modern application, we support your choice to blind your eyes, or work in the dark"
                        ),
                        Tile(
                            icon="radix-icons:person",
                            heading="Support",
                            description="Community support offered by The Open Mining Initiative: proof of work advocates"
                        ),
                    ],
                )
            ],
        ),
        dmc.Space(h=50),
        create_title("Users", id="users"),
        create_head(
            dmc.Anchor(
                "Become a user",
                underline=False,
                href="https://gitlab.com/open-mining-initiative/",
            )
        ),
        dmc.Space(h=40),
        dmc.Center(
            children=[
                dmc.Group(
                    spacing="xs",
                    children=[
                        dmc.Text("Made with pure"),
                        DashIconify(icon="akar-icons:python-fill",
                                    width=20, color="light"),
                        dmc.Text("and"),
                        DashIconify(icon="akar-icons:heart",
                                    width=20, color="pink"),

                        dmc.Text("by the Open Mining Initiative"),
                    ],
                )
            ],
            style={"height": 100},
        ),
    ]
)
