
# 3rd party
import dash
import dash_mantine_components as dmc
from dash import html

# package
from stage_zero_dashboard.components.tabs import TabbedContent
from stage_zero_dashboard.data.fetched_data import RemoteData
from stage_zero_dashboard.data.local_data import HashRateData

dash.register_page(
    __name__,
    "/pool-analysis",
    title="Pool Analysis",
    description="Research and Development dashboard for mining pool analysis, revenue streams, and crypto mining news and price updates",
)


layout = html.Section(
    children=[

        TabbedContent(RemoteData().data, HashRateData().data),
    ]
)
